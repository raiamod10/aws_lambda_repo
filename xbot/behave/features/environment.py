from behave import fixture
from xbot import config
from behave.fixture import use_fixture_by_tag
import boto3
import pprint
import os
import random


@fixture
def lambda_function(context):
    context.FunctionName = config.FunctionName
    client = boto3.client('lambda')
    with open(r'xbot\behave\features\code.zip', 'rb') as f:
        zipped_code = f.read()
    response = client.create_function(
        FunctionName=context.FunctionName,
        Runtime='python3.7',
        Role='arn:aws:iam::360752793804:role/LambdaRole',
        Code=dict(ZipFile=zipped_code),
        Handler='lambda_function.lambda_handler',
        Description='Some Description',
        Timeout=123,
        MemorySize=200,
        Publish=True
    )
    pprint.pprint(response)
    context.lambda_function_id = response['FunctionArn'] +':'+ response['Version']

    Cross_Account_cmd = f'aws lambda add-permission --function-name {context.lambda_function_id} ' \
        f'--action lambda:InvokeFunction --statement-id s3-account{random.randint(2, 100)} --principal ' \
        's3.amazonaws.com --source-arn arn:aws:s3:::amoddemobucket --source-account 420752799804 ' \
        '--output text'
    os.system(Cross_Account_cmd)
    print('Lambda Function Arn :', context.lambda_function_id)
    context.lambda_function_id = response['FunctionArn']

    # yield
    # client.delete_function(
    #     FunctionName=context.FunctionName,
    # )


fixture_registry = {
    "fixture.lambda_function": lambda_function
}


def before_tag(context, tag):
    if tag.startswith("fixture."):
        return use_fixture_by_tag(tag, context, fixture_registry)